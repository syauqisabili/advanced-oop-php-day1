<?php

require_once("Hewan.php");

trait Fight {
    use Hewan;

    public $attackPower;
    public $deffencePower;

    public function serang($target){
        echo "{$this->nama} sedang menyerang {$target->nama} <br> <br>";
    }

    public function diserang($penyerang){
        $this->darah -= $penyerang->attackPower / $this->deffencePower;
    }
    
}