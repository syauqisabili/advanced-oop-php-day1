<?php

require_once("Hewan.php");
require_once("Fight.php");

class Elang {
    
    use Hewan, Fight;
    
    public function __construct($nama){
        
        $this->nama             = $nama;
        $this->keahlian         = "terbang tinggi";
        $this->jumlahKaki       = 2;
        $this->attackPower      = 10;
        $this->deffencePower    = 5;
    }

    public function getInfoHewan(){
        echo "  Nama        : {$this->nama} <br>
                Darah       : {$this->darah} <br>
                Keahlian    : {$this->keahlian} <br>
                Jumlah Kaki : {$this->jumlahKaki} <br>
                Attack      : {$this->attackPower} <br>
                Deffence    : {$this->deffencePower} <br>   
                    ";
    }

}