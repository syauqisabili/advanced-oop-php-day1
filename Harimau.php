<?php

require_once("Hewan.php");
require_once("Fight.php");

class Harimau{
    
    use Hewan, Fight;

    public function __construct($nama){
        
        $this->nama             = $nama;
        $this->keahlian         = "lari cepat";
        $this->jumlahKaki       = 4;
        $this->attackPower      = 7;
        $this->deffencePower    = 8;
    }

    public function getInfoHewan(){
        echo "  Nama        : {$this->nama} <br>
                Darah       : {$this->darah} <br>
                Keahlian    : {$this->keahlian} <br>
                Jumlah Kaki : {$this->jumlahKaki} <br>
                Attack      : {$this->attackPower} <br>
                Deffence    : {$this->deffencePower} <br>   
                    ";
    }

}