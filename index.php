<?php


require_once("Elang.php");
require_once("Harimau.php");

$Elang1 = new Elang("Elang 1");
$Elang1->getInfoHewan();
echo "<br>";

$Harimau1 = new Harimau("Harimau 1");
$Harimau1->getInfoHewan();

echo "<br>";
$Elang1->serang($Harimau1);
$Harimau1->diserang($Elang1);
$Harimau1->getInfoHewan();

echo "<br>";
echo "<br>";
$Harimau1->serang($Elang1);
$Elang1->diserang($Harimau1);
$Elang1->getInfoHewan();
